# WODTogether 3.0

This public repository will be for issue tracking during development of the next version of WODTogether.

## Contributing

If you're interested in helping build the next version of WODTogether, please contact bholub@wodtogether.com 

WODTogether 3 consists of 2 main projects: API (PHP/Laravel, MySQL) and UI (Vue/Nuxt/Vuetify). We'd also love help with automated testing, manual testing, devops, project management, and support.